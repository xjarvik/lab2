//
//  ExperienceViewController.swift
//  Lab2
//
//  Created by William Söder on 2019-11-06.
//  Copyright © 2019 William Söder. All rights reserved.
//

import UIKit

class ExperienceViewController: UITableViewController {
    //MARK: Properties
     
    var work = [Experience]()
    var education = [Experience]()
    var selectedSection: Int = 0
    var selectedRow: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        loadSampleExperience()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return work.count
        }
        else{
            return education.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ExperienceCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ExperienceTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ExperienceTableViewCell.")
        }
        
        var exp:Experience
        if(indexPath.section == 0){
            exp = work[indexPath.row]
        }
        else{
            exp = education[indexPath.row]
        }
        
        cell.experienceLabel.text = exp.work
        cell.experienceImage.image = exp.image
        cell.yearsLabel.text = exp.years
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0){
            return "Work"
        }
        else{
            return "Education"
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedSection = indexPath.section
        selectedRow = indexPath.row
        performSegue(withIdentifier: "ShowExperienceDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        if segue.destination is ExperienceDetailViewController
        {
            let vc = segue.destination as? ExperienceDetailViewController
            
            if(selectedSection == 0){
                vc?.selectedExperience = work[selectedRow]
            }
            else{
                vc?.selectedExperience = education[selectedRow]
            }
        }
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: Private Methods
     
    private func loadSampleExperience() {
        let e1 = Experience(work: "Feriepraktik @ Tekniska Kontoret, Huskvarna", years: "Summer 2017", image: UIImage(systemName: "paintbrush.fill"), desc: """
        Feriepraktik at Tekniska Kontoret in Huskvarna. Gardening, painting, among other things.
        """)
         
        let e2 = Experience(work: "Feriepraktik @ Tekniska Kontoret, Huskvarna", years: "Summer 2016", image: UIImage(systemName: "paintbrush.fill"), desc: """
        Feriepraktik at Tekniska Kontoret in Huskvarna. Gardening, painting, among other things.
        """)
         
        let e3 = Experience(work: "Summer job @ Engelska Skolan, Huskvarna", years: "Summer 2015", image: UIImage(systemName: "paintbrush.fill"), desc: """
        Summer job at Engelska Skolan in Huskvarna. Mostly painting, repainted most walls inside the school.
        """)
        
        let e4 = Experience(work: "Praktik @ TMG Tabergs", years: "Spring 2015", image: UIImage(systemName: "paintbrush.fill"), desc: """
        School "prao" at TMG Tabergs, a company mostly focused around printing papers and leaflets for other companies. Mostly learned about their workflow.
        """)
                 
        let e5 = Experience(work: "Praktik @ Huskvarna Folkets Park", years: "Spring 2014", image: UIImage(systemName: "paintbrush.fill"), desc: """
        School "prao" at Huskvarna Folkets Park. Various activities ranging from putting up posters to cleaning the facility.
        """)
        
        work += [e1, e2, e3, e4, e5]
         
        let e6 = Experience(work: "Datateknik: Mjukvaruutveckling och mobila plattformar @ JTH", years: "2018-now", image: UIImage(systemName: "pencil"), desc: """
        Where i'm at right now!
        """)
        
        let e7 = Experience(work: "Gymnasie: Teknikprogrammet @ ED", years: "2015-2018", image: UIImage(systemName: "pencil"), desc: """
        Studied the fundamentals in web development, programming and also some designing.
        """)
        
        let e8 = Experience(work: "Grundskola: Engelska Skolan", years: "2011-2015", image: UIImage(systemName: "pencil"), desc: """
        Typical swedish education.
        """)
        
        let e9 = Experience(work: "Grundskola: Landsjöskolan", years: "2006-2011", image: UIImage(systemName: "pencil"), desc: """
        Typical swedish education.
        """)
        
        education += [e6, e7, e8, e9]
    }

}
