//
//  ExperienceDetailViewController.swift
//  Lab2
//
//  Created by William Söder on 2019-11-06.
//  Copyright © 2019 William Söder. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var detailWorkLabel: UILabel!
    @IBOutlet weak var detailYearsLabel: UILabel!
    @IBOutlet weak var detailDescLabel: UILabel!
    
    var selectedExperience = Experience(work: "Chicken and Potatoes", years: "2018-2019", image: nil, desc: """
            This is a test.
    """)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = selectedExperience.work
        detailImage.image = selectedExperience.image
        detailWorkLabel.text = selectedExperience.work
        detailYearsLabel.text = selectedExperience.years
        detailDescLabel.text = selectedExperience.desc
        detailDescLabel.sizeToFit()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
