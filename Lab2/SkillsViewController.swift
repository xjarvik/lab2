//
//  SkillsViewController.swift
//  Lab2
//
//  Created by William Söder on 2019-11-06.
//  Copyright © 2019 William Söder. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    @IBOutlet weak var animationView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let startPosition = self.view.center
        animationView.center = startPosition
        
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseInOut, .preferredFramesPerSecond60], animations: {
            self.animationView.superview?.layoutIfNeeded()
            self.animationView.backgroundColor = UIColor.red
        }) { (completed) in
            if completed {
                self.advancedAnimation()
            }
            
        }
    }
    
    func advancedAnimation() {
        let startPosition = self.view.center
        UIView.animateKeyframes(withDuration: 1, delay: 0, options: [.repeat, .autoreverse], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1) {
                self.animationView.backgroundColor = UIColor.green
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2) {
                self.animationView.center = CGPoint(x: startPosition.x, y: 50)
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.1) {
                self.animationView.transform = CGAffineTransform(rotationAngle: CGFloat.pi).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.6) {
                self.animationView.transform = CGAffineTransform.identity
            }
        }) { (completed) in
            print("Completed: \(completed)")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
