//
//  CVViewController.swift
//  Lab2
//
//  Created by William Söder on 2019-11-06.
//  Copyright © 2019 William Söder. All rights reserved.
//

import UIKit

class CVViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func experienceButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "ShowExperience", sender: self)
    }
    
    @IBAction func skillsButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "ShowSkills", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
