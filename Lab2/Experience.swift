//
//  Experience.swift
//  Lab2
//
//  Created by William Söder on 2019-11-21.
//  Copyright © 2019 William Söder. All rights reserved.
//

import UIKit

class Experience: NSObject {
    var work: String
    var years: String
    var image: UIImage?
    var desc: String
    
    init(work: String, years: String, image: UIImage?, desc: String){
        self.work = work
        self.years = years
        self.image = image
        self.desc = desc
    }
}
