//
//  ExperienceTableViewCell.swift
//  Lab2
//
//  Created by William Söder on 2019-11-06.
//  Copyright © 2019 William Söder. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    //MARK: Properties
    
    @IBOutlet weak var experienceLabel: UILabel!
    
    @IBOutlet weak var yearsLabel: UILabel!
    
    @IBOutlet weak var experienceImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
